pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

rootProject.name = "Crossplatform-Test"

include(":shared")
include(":composeDesktopApp")
include(":androidApp")
include(":reactJsApp")
include(":composeWebApp")
