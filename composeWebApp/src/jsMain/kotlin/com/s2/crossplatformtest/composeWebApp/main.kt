package com.s2.crossplatformtest.composeWebApp

import com.s2.crossplatformtest.composeWebApp.compose.views.MainScreen
import kotlinx.browser.window
import org.jetbrains.compose.web.renderComposable

fun main() {
    window.onload = {
        renderComposable(rootElementId = "root") {
            MainScreen()
        }
    }
}
