package com.s2.crossplatformtest.composeWebApp.compose.views

import androidx.compose.runtime.*
import com.s2.crossplatformtest.shared.CounterViewModel
import com.s2.crossplatformtest.shared.CounterViewModelImpl
import com.s2.crossplatformtest.shared.CounterViewState
import org.jetbrains.compose.common.material.Text

class ViewState : CounterViewState {
    override var counter by mutableStateOf("Loading ...")
}

@Composable
fun Counter() {
    val viewModel: CounterViewModel<ViewState> = remember {
        CounterViewModelImpl(ViewState())
    }

    DisposableEffect(Unit) {
        viewModel.startUpdates()
        viewModel.startCounting()
        onDispose {
            viewModel.stopUpdates()
        }
    }

    Text("Counter: ${viewModel.viewState.counter}")
}
