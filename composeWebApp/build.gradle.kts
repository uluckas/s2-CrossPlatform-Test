import org.jetbrains.compose.compose
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

plugins {
    kotlin("multiplatform")

    // Use fully qualified name for buildSrc imports
    // https://github.com/gradle/gradle/issues/9270
    id("org.jetbrains.compose") version com.s2.crossplatformtest.buildsrc.DependencyVersion.composeJb
}

group = "com.s2s"
version = "1.0-SNAPSHOT"

kotlin {
    js(IR) {
        compilations.all {
            kotlinOptions {
                sourceMap = true
                freeCompilerArgs += "-Xopt-in=org.jetbrains.compose.common.ui.ExperimentalComposeWebWidgetsApi"
            }
        }
        browser {
            webpackTask {
                sourceMaps = true
                cssSupport.enabled = true
            }
            runTask {
                sourceMaps = true
                cssSupport.enabled = true
            }
            testTask {
                useKarma {
                    useChromeHeadless()
                    webpackConfig.cssSupport.enabled = true
                }
            }
        }
        binaries.executable()
    }

    sourceSets {
        val jsMain by getting {
            dependencies {
                implementation(compose.web.widgets)
                //implementation(compose.uiTooling)
                implementation(project(":shared"))
            }
        }
    }
}
