import SwiftUI
import shared

class ViewState: CounterViewState, ObservableObject {
    @Published var counter = "Loading ..."
}

struct CounterView: View {
    @StateObject var viewState: ViewState
    @State var viewModel: CounterViewModel

    init() {
        let state = ViewState()
        _viewState = StateObject(wrappedValue: state)
        _viewModel = State(wrappedValue: CounterViewModelImpl(viewState: state))
    }

    var body: some View {
        VStack {
            Text(viewState.counter)
        }.attachViewModel(self.viewModel)
        .onAppear {
            self.viewModel.startCounting()
        }
    }
}

struct CounteView_Previews: PreviewProvider {

    static var previews: some View {
        CounterView()
    }
}


