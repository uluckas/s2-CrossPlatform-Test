//
//  ViewExtensions.swift
//  iosApp
//
//  Created by ulrich.luckas on 20.05.21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

extension View {
    func attachViewModel(_ viewModel: BaseViewModel) -> some View {
        return onAppear {
            viewModel.startUpdates()
        }.onDisappear {
            viewModel.stopUpdates()
        }
    }
}
