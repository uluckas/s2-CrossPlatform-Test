import com.s2.crossplatformtest.buildsrc.DependencyVersion

plugins {
    kotlin("multiplatform")
}

group = "com.s2s"
version = "1.0-SNAPSHOT"

repositories {
    maven { url = uri("https://maven.pkg.jetbrains.space/kotlin/p/kotlin/kotlin-js-wrappers") }
}

kotlin {
    js(IR) {
        compilations.all {
            kotlinOptions {
                sourceMap = true
            }
        }
        browser {
            binaries.executable()
            webpackTask {
                sourceMaps = true
                cssSupport.enabled = true
            }
            runTask {
                sourceMaps = true
                cssSupport.enabled = true
            }
            testTask {
                useKarma {
                    useChromeHeadless()
                    webpackConfig.cssSupport.enabled = true
                }
            }
        }
    }
    sourceSets {
        val jsMain by getting {
            dependencies {
                implementation(project.dependencies.enforcedPlatform("org.jetbrains.kotlin-wrappers:kotlin-wrappers-bom:${DependencyVersion.kotlinWrappersVersion}"))
                implementation(project(":shared"))
                //implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${DependencyVersion.kotlinCoroutines}-native-mt")
                implementation("org.jetbrains.kotlin-wrappers:kotlin-react") // Version through kotlin-wrappers-bom
                implementation("org.jetbrains.kotlin-wrappers:kotlin-react-dom") // Version through kotlin-wrappers-bom
            }
        }
        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
    }
}
