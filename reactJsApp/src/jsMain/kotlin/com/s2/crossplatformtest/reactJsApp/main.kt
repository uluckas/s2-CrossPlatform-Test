package com.s2.crossplatformtest.reactJsApp

import com.s2.crossplatformtest.reactJsApp.react.views.counter
import kotlinx.browser.document
import kotlinx.browser.window
import react.dom.render

fun main() {
    window.onload = {
        render(document.getElementById("root")) {
            counter {
                attrs {
                    //counter = "Loading ..."
                }
            }
        }
    }
}
