package com.s2.crossplatformtest.reactJsApp.utils

import com.s2.crossplatformtest.shared.CounterViewState
import react.*
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty

/**
 * Property delegate for holding React State in a Kotlin property.
 *
 * @param T Type of delegated property
 *
 * @param initialValue Initial value of the state propertty
 */
class KotlinState<T>(initialValue: T) {

    val state = useState(initialValue)

    operator fun getValue(parent: Any?, property: KProperty<*>): T {
        val (state, _) = state
        return state
    }

    operator fun setValue(parent: Any?, property: KProperty<*>, value: T) {
        val (_, setter) = state
        setter(value)
    }
}

fun <T> KotlinState() = KotlinState<T?>(null)

/**
 * Hook to use a [KotlinState] property
 */
fun useKotlinState(property: KMutableProperty0<String>) = Pair(property.get(), property::set)
