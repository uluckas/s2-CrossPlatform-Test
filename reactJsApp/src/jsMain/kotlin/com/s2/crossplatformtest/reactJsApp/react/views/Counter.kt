package com.s2.crossplatformtest.reactJsApp.react.views

import com.s2.crossplatformtest.reactJsApp.utils.KotlinState
import com.s2.crossplatformtest.reactJsApp.utils.useKotlinState
import com.s2.crossplatformtest.shared.CounterViewModelImpl
import com.s2.crossplatformtest.shared.CounterViewState
import react.PropsWithChildren
import react.dom.br
import react.dom.div
import react.functionComponent
import react.useEffectOnce

interface CounterProps : PropsWithChildren {

    var initialCounter: String?
}

class CounterStateDelegate : CounterViewState {

    override var counter by KotlinState("Loading ...")
}

val counter = functionComponent<CounterProps> {

    val viewModel = CounterViewModelImpl(CounterStateDelegate())
    val viewState = viewModel.viewState

    useEffectOnce {
        viewModel.startUpdates()
        viewModel.startCounting()
        cleanup {
            viewModel.stopUpdates()
        }
    }

    val (counter, _) = useKotlinState(viewState::counter)

    div {
        +"Hello World"
        br {}
        +counter
    }
}


