buildscript {
    repositories {
        gradlePluginPortal()
        google()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:${com.s2.crossplatformtest.buildsrc.DependencyVersion.androidGradlePlugin}")
        classpath(kotlin("gradle-plugin", version = com.s2.crossplatformtest.buildsrc.DependencyVersion.kotlin))
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}
