import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("multiplatform")

    // Use fully qualified name for buildSrc imports
    // https://github.com/gradle/gradle/issues/9270
    id("org.jetbrains.compose") version com.s2.crossplatformtest.buildsrc.DependencyVersion.composeJb
}

kotlin {
    jvm {
        //withJava()
    }
    sourceSets {
        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(compose.desktop.currentOs)
                implementation(compose.uiTooling)

                implementation(compose.ui)
                implementation(compose.runtime)

                implementation(project(":shared"))
            }
        }
    }
}

compose.desktop {
    application {
        mainClass = "com.s2.crossplatformtest.composeDesktopApp.MainKt"

        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "ComposeCodeViewer"
            packageVersion = "1.0.0"

            windows {
                menu = true
                // see https://wixtoolset.org/documentation/manual/v3/howtos/general/generate_guids.html
                upgradeUuid = "AF792DA6-2EA3-495A-95E5-C3C6CBCB9948"
            }

            macOS {
                // Use -Pcompose.desktop.mac.sign=true to sign and notarize.
                bundleID = "com.s2.s2crossplatform.composeWebApp"
            }
        }
    }
}

/*tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        freeCompilerArgs += arrayOf(
            "-P", "plugin:androidx.compose.compiler.plugins.kotlin:suppressKotlinVersionCompatibilityCheck=true"
        )
    }
}*/
