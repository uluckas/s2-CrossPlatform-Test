package com.s2.crossplatformtest.composeDesktopApp

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.runtime.Composable
import com.s2.crossplatformtest.shared.compose.views.Counter

@Preview
@Composable
fun CounterPreview() = Counter()
