package com.s2.crossplatformtest.composeDesktopApp

import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import com.s2.crossplatformtest.shared.compose.views.MainScreen

fun main(/*args: Array<String>*/) = application {
    Window(
        onCloseRequest = ::exitApplication,
        title = "Cross Platform Test",
        state = rememberWindowState(width = 300.dp, height = 300.dp)
    ) {
        MainScreen()
    }
}
