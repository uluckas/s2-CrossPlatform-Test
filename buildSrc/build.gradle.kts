plugins {
    `kotlin-dsl`
}

repositories {
    google()
    mavenCentral()
}

/*
 * Override outdated kotlin dependencies from kotlin-dsl plugin to work around conflicts with
 * compose
 */
dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.5.30")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.5.30")
}
