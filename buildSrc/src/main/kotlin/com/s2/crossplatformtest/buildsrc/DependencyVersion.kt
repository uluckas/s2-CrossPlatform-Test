package com.s2.crossplatformtest.buildsrc

object DependencyVersion {

    const val androidGradlePlugin = "7.1.0-alpha13"   // For latest AS Canary
    //const val androidGradlePlugin = "7.0.2"           // For latest AS
    //const val androidGradlePlugin = "4.2.0"           // For latest IntelliJ EAP - For Web Support
    //const val androidGradlePlugin = "4.2.0"           // For latest IntelliJ - For Web Support

    const val kotlin = "1.5.31"

    const val kotlinCoroutines = "1.5.2-native-mt"

    const val composeJb = "1.0.0-alpha4-build366"

    const val jetpackCompose = "1.0.1"

    // JS wrapper version for React
    const val kotlinWrappersVersion = "0.0.1-pre.249-kotlin-1.5.31"
}
