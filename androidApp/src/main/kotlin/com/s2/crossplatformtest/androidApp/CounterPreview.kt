package com.s2.crossplatformtest.androidApp

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.s2.crossplatformtest.shared.compose.views.Counter

@Preview(showBackground = true)
@Composable
fun CounterPreview() = Counter()
