import com.s2.crossplatformtest.buildsrc.DependencyVersion
import org.jetbrains.compose.compose

plugins {
    id("com.android.application")

    kotlin("android")

    // Use fully qualified name for buildSrc imports
    // https://github.com/gradle/gradle/issues/9270
    id("org.jetbrains.compose") version com.s2.crossplatformtest.buildsrc.DependencyVersion.composeJb
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("androidx.appcompat:appcompat:1.3.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${DependencyVersion.kotlinCoroutines}")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.3.1")

    implementation("androidx.activity:activity-compose:1.3.1")

    implementation(compose.ui)
    // Tooling support (Previews, etc.)
    implementation(compose.uiTooling)
    // Foundation (Border, Background, Box, Image, Scroll, shapes, animations, etc.)
    implementation(compose.foundation)
    //implementation("androidx.compose.foundation:foundation:${DependencyVersion.jetpackCompose}")
    // Material Design
    implementation(compose.material)
    //implementation("androidx.compose.material:material:${DependencyVersion.jetpackCompose}")
    // Material design icons
    implementation(compose.materialIconsExtended)
    //implementation("androidx.compose.material:material-icons-core:${DependencyVersion.jetpackCompose}")
    //implementation("androidx.compose.material:material-icons-extended:${DependencyVersion.jetpackCompose}")
    implementation(compose.runtime)
    //implementation("androidx.compose.runtime:runtime:${DependencyVersion.jetpackCompose}")

    implementation(project(":shared"))

    // UI Tests
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:1.0.2")
}

android {
    compileSdk = 31
    defaultConfig {
        applicationId = "com.s2.s2_crossplatform_test.androidApp"
        minSdk = 21
        targetSdk = 30
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    buildFeatures {
        // Enables Jetpack Compose for this module
        compose = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    composeOptions {
        // Needed for AGP versions < 7.0.0
        kotlinCompilerVersion = DependencyVersion.kotlin
    }
    /*kotlinOptions {
        freeCompilerArgs += arrayOf(
            "-P", "plugin:androidx.compose.compiler.plugins.kotlin:suppressKotlinVersionCompatibilityCheck=true",
        )
    }*/
}
