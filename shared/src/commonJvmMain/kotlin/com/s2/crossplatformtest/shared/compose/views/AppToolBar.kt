package com.s2.crossplatformtest.shared.compose.views

import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable

@Composable
expect fun AppToolBar()