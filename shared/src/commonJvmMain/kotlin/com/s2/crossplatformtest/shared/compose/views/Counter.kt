package com.s2.crossplatformtest.shared.compose.views

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.s2.crossplatformtest.shared.CounterViewModel
import com.s2.crossplatformtest.shared.CounterViewModelImpl
import com.s2.crossplatformtest.shared.CounterViewState

class ViewState() : CounterViewState {

    override var counter by mutableStateOf("Loading ...")
}

@Composable
fun Counter() {
    val viewModel: CounterViewModel<ViewState> = remember {
        CounterViewModelImpl(ViewState())
    }

    DisposableEffect(Unit) {
        viewModel.startUpdates()
        viewModel.startCounting()
        onDispose {
            viewModel.stopUpdates()
        }
    }

    Text("Counter: ${viewModel.viewState.counter}")
}
