package com.s2.crossplatformtest.shared

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

data class AppState(val counter: Int = 0) {
}

internal val mutableAppStateFlow = MutableStateFlow(AppState())

val appStateFlow = mutableAppStateFlow.asStateFlow()
