package com.s2.crossplatformtest.shared

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

interface BaseViewModel {

    fun startUpdates()

    fun stopUpdates()
}

abstract class BaseViewModelImpl : BaseViewModel {

    protected var viewModelScope: CoroutineScope? = null

    override fun startUpdates() {
        CoroutineScope(SupervisorJob() + Dispatchers.Main).let { newScope ->
            viewModelScope?.cancel()
            viewModelScope = newScope
            appStateFlow
                .onEach { appState ->
                    updateViewState(appState)
                }
                .launchIn(newScope)
        }
    }

    override fun stopUpdates() {
        viewModelScope?.cancel()
    }

    abstract fun updateViewState(appState: AppState)
}
