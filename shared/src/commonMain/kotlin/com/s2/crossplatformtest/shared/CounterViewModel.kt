package com.s2.crossplatformtest.shared

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

interface CounterViewState {

    var counter: String
}

interface CounterViewModel<VS : CounterViewState> : BaseViewModel {

    val viewState: VS
    fun startCounting()
}

class CounterViewModelImpl<VS : CounterViewState>(override val viewState: VS) : BaseViewModelImpl(), CounterViewModel<VS> {

    override fun startCounting() {
        viewModelScope?.launch {
            while (true) {
                delay(1000)
                val appState = appStateFlow.value
                mutableAppStateFlow.value = appState.copy(counter = appState.counter + 1)
            }
        }
    }

    override fun updateViewState(appState: AppState) {
        viewState.counter = "Counting: ${appState.counter}"
    }
}
