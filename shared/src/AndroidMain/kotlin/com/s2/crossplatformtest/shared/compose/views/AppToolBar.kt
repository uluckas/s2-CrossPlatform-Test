package com.s2.crossplatformtest.shared.compose.views

import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable

@Composable
actual fun AppToolBar() {
    TopAppBar(
        title = {
            Text(text = "CrossPlatform Test Android")
        },
        //                        navigationIcon = {
        //                            IconButton(onClick = { }) {
        //                                Icon(Icons.Filled.Menu,"")
        //                            }
        //                        },
        //                        backgroundColor = Color.Blue,
        //                        contentColor = Color.White,
        //                        elevation = 12.dp
    )
}