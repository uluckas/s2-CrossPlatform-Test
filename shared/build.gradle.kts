import com.s2.crossplatformtest.buildsrc.DependencyVersion
import org.jetbrains.compose.compose
import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    kotlin("multiplatform")

    // Use fully qualified name for buildSrc imports
    // https://github.com/gradle/gradle/issues/9270
    id("org.jetbrains.compose") version com.s2.crossplatformtest.buildsrc.DependencyVersion.composeJb
    id("com.android.library")
}

kotlin {
    android()
    jvm("desktop")
    val iosTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget = when {
        System.getenv("SDK_NAME")?.startsWith("iphoneos") == true -> ::iosArm64
        System.getenv("NATIVE_ARCH")?.startsWith("arm") == true -> ::iosSimulatorArm64
        else -> ::iosX64
    }
    iosTarget("ios") {
        compilations.all {
            kotlinOptions {
                freeCompilerArgs += "-Xexport-kdoc"
            }
        }
        binaries {
            framework {
                //export("org.jetbrains.kotlinx:kotlinx-coroutines-core:${DependencyVersion.kotlinCoroutines}")
                //transitiveExport = true
                baseName = "shared"
            }
        }
    }
    js(IR) {
        compilations.all {
            kotlinOptions {
                sourceMap = true
            }
        }
        browser {
            webpackTask {
                sourceMaps = true
                cssSupport.enabled = true
            }
            runTask {
                sourceMaps = true
                cssSupport.enabled = true
            }
            testTask {
                useKarma {
                    useChromeHeadless()
                    webpackConfig.cssSupport.enabled = true
                }
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${DependencyVersion.kotlinCoroutines}")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val commonJvmMain by creating {
            dependsOn(commonMain)
            dependencies {
                implementation(compose.foundation)
                implementation(compose.ui)
                /* Not yet ready. */
                //implementation(compose.uiTooling)
                implementation(compose.material)
                implementation(compose.materialIconsExtended)
                implementation(compose.runtime)
            }
        }
        val commonJvmTest by creating {
            dependsOn(commonTest)
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13.2")
            }
        }
        val desktopMain by getting {
            dependsOn(commonJvmMain)
        }
        val desktopTest by getting {
            dependsOn(commonJvmTest)
        }
        val androidMain by getting {
            dependsOn(commonJvmMain)
        }
        // Workaround for:
        // The Kotlin source set androidAndroidTestRelease was configured but not added to any
        // Kotlin compilation. You can add a source set to a target's compilation by connecting it
        // with the compilation's default source set using 'dependsOn'.
        // See https://kotlinlang.org/docs/reference/building-mpp-with-gradle.html#connecting-source-sets
        //
        // This workaround includes `dependsOn(androidAndroidTestRelease)` in the `androidTest` sourceSet.
        val androidAndroidTestRelease by getting
        // And more for Bumblebee
        //val androidTestFixtures by getting
        //val androidTestFixturesDebug by getting
        //val androidTestFixturesRelease by getting
        val androidTest by getting {
            dependsOn(commonJvmTest)
            dependsOn(androidAndroidTestRelease)
            //dependsOn(androidTestFixtures)
            //dependsOn(androidTestFixturesDebug)
            //dependsOn(androidTestFixturesRelease)
        }
        val iosMain by getting
        val iosTest by getting
        val jsMain by getting {
            dependencies {
                implementation(compose.runtime)
            }
        }
        val jsTest by getting
    }
}

android {
    compileSdk = 30
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdk = 21
        targetSdk = 30
    }
}

/*tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        //jvmTarget = "1.8"
        //useIR = true
        freeCompilerArgs += arrayOf(
            "-P", "plugin:androidx.compose.compiler.plugins.kotlin:suppressKotlinVersionCompatibilityCheck=true"
        )
    }
}*/

val packForXcode by tasks.creating(Sync::class) {
    group = "build"
    val mode = System.getenv("CONFIGURATION") ?: "DEBUG"
    val framework = kotlin.targets.getByName<KotlinNativeTarget>("ios").binaries.getFramework(mode)
    inputs.property("mode", mode)
    dependsOn(framework.linkTask)
    val targetDir = File(buildDir, "xcode-frameworks")
    from({ framework.outputDirectory })
    into(targetDir)
}
